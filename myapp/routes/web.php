<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\TaskController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::prefix('tasks')->group(function(){
    Route::get('/',[TaskController::class,'index'])->name('tasks.index');

    Route::get('/search', [TaskController::class,'search'])->name('tasks.search');

    Route::middleware('auth')->group(function(){
        Route::get('/create', [TaskController::class,'create'])->name('tasks.create');
        Route::post('/store', [TaskController::class,'store'])->name('tasks.store');
        Route::get('/edit/{id}', [TaskController::class,'edit'])->name('tasks.edit');
        Route::put('/update/{id}', [TaskController::class,'update'])->name('tasks.update');
        Route::delete('/destroy/{id}', [TaskController::class,'destroy'])->name('tasks.destroy');
    });

});
