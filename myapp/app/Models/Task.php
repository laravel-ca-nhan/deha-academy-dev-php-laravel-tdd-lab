<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    public $table = 'tasks';

    protected $fillable = ['name','content'];

    static function search($keyword)
    {
        return self::where('name', 'LIKE', '%'.$keyword.'%')
                ->orWhere('content', 'LIKE', '%'.$keyword.'%')->orderBy('id','desc')->paginate(20)->withQueryString();
    }

}
