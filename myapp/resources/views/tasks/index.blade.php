@extends('layouts.app')

@section('content')

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <form class="d-flex m-lg-2 flex-grow-1" id="search-form" action="{{ route('tasks.search') }}">
        {{-- @csrf --}}
        <input class="form-control me-2 flex-grow-1" style="width: 300px" type="search" name="keyword"
            placeholder="Search" aria-label="Search" value="{{ isset($keyword) ? $keyword : '' }}">
        <button class="btn btn-outline-success" id="search_btn" type="submit">Search</button>
    </form>
    <div class="container-fluid">
        <div class="d-flex justify-content-end w-100">
            <button class="btn btn-primary ms-3" type="button"><a class="nav-link"
                    href="{{ route('tasks.create') }}">Create New Task</a></button>
        </div>
    </div>
</nav>
@if ($tasks->count() <= 0)
    <tr>
        <td colspan="4" class="text-center py-4">
            <div class="alert alert-warning text-center">
                <h4 class="mb-0">Không tồn tại task này</h4>
            </div>
        </td>
    </tr>
@else
<table class="table table-striped table-hover">
    <thead>
        <tr>
            <th scope="col">Id</th>
            <th scope="col">Name</th>
            <th scope="col">Content</th>
            @if (Auth::check())
            <th scope="col">Actions</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @foreach ($tasks as $task)
            <tr>
                <th scope="row">{{ $task->id }}</th>
                <td>{{ $task->name }}</td>
                <td style="white-space: pre-wrap;">{{ $task->content }}</td>
                <td>
                    @if (Auth::check())
                    <a href=" {{ route('tasks.edit',$task->id) }} " class="btn btn-primary btn-sm">Update</a>
                    <form id="form-delete" action="{{ route('tasks.destroy',$task->id) }}" method="POST"
                        style="display: inline-block;">
                        @csrf
                        @method('DELETE')
                        <button type="submit" id="delete-btn" class="btn btn-danger btn-sm"
                            onclick="return confirm('Bạn chắc chắn muốn xoá ?')">Delete</button>
                    </form>
                    @endif
                </td>

            </tr>
        @endforeach
    </tbody>
</table>
@endif
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center">
        {{ $tasks->links() }}
    </ul>
</nav>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
@endsection
