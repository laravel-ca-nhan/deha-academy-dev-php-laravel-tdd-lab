@extends('layouts.app')
@section('content')
    <div class="container my-5">
        <h1>Update Task</h1>
        <form method="POST" action="{{ route('tasks.update',$task->id) }}">
            @csrf
            @method('put')
            <div class="mb-3">
                <label for="name" class="form-label">Name:</label>
                <input type="text" class="form-control" id="name" name="name" value="{{ $task->name }}">
                <div class="errors">
                    @error('name')
                        <i class="error text-danger"> {{ $message }} </i>
                    @enderror
                </div>
            </div>
            <div class="mb-3">
                <label for="content" class="form-label">Content:</label>
                <textarea class="form-control" id="content" name="content" rows="5" >{{ $task->content }}</textarea>
            </div>
            <button type="submit" class="btn btn-primary">Update Task</button>
        </form>
    </div>
@endsection
