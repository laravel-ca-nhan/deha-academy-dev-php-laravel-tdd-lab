<?php

namespace Tests\Feature;

use App\Models\Task;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;
use Illuminate\Support\Str;
class SearchTaskTest extends TestCase
{
    public function searchTaskRoute($argument)
    {
        return route('tasks.search',$argument);
    }
    /** @test */
    public function user_can_search_task_by_name()
    {
        $task = Task::factory()->create(['name' => 'Test Task']);
        $response = $this->get($this->searchTaskRoute($task->name));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText($task->name);
    }

    /** @test */
    public function user_can_search_task_by_content()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->searchTaskRoute($task->content));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText($task->name);
    }

    /** @test */
    public function user_can_search_task_when_name_is_upper()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->searchTaskRoute(Str::upper($task->name)));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertSeeText($task->name);
    }

}
