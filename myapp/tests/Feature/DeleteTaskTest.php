<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{


    /** @test*/
    public function authenticate_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->delete('/tasks/destroy/'.$task->id);
        $response->assertRedirect(route('tasks.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test*/
    public function unauthenticate_user_can_not_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete('/tasks/destroy/'.$task->id);
        $response->assertRedirect(route('login'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test*/
    public function authenticate_user_can_not_delete_task_when_id_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $task_id = -1;
        $response = $this->delete('/tasks/destroy/'.$task_id);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
