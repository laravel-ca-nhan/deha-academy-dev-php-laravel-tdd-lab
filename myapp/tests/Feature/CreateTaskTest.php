<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class CreateTaskTest extends TestCase
{
    public function createTaskViewRoute()
    {
        return route('tasks.create');
    }

    public function createTaskRoute()
    {
        return route('tasks.store');
    }

    /** @test */
    public function authenticate_user_can_create_new_task()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make();
        $response = $this->post($this->createTaskRoute(),$task->toArray());
        $this->assertDatabaseHas('tasks',$task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/tasks');
    }

    /** @test */
    public function unauthenticate_user_can_not_create_new_task()
    {
        $task = Task::factory()->make();
        $response = $this->post($this->createTaskRoute(),$task->toArray());
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect('/login');
    }

    /** @test */
    public function authenticate_user_can_not_create_new_task_if_empty_name_field()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->make(['name' => null]);
        $response = $this->post($this->createTaskRoute(),$task->toArray());
        $response->assertSessionHasErrors('name');
    }

    /** @test */
    public function authenticate_user_can_access_create_form()
    {
        $this->actingAs(User::factory()->create());
        $response = $this->get($this->createTaskViewRoute());
        $response->assertViewIs('tasks.create');
        $response->assertStatus(Response::HTTP_OK);
    }

    /** @test */
    public function unauthenticate_user_can_not_access_create_form()
    {
        $response = $this->get($this->createTaskViewRoute());
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

}
