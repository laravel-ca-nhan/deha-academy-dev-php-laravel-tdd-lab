<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{

    public function updateTaskRoute($id)
    {
        return route('tasks.update', $id);
    }

    public function updateTaskViewRoute($id)
    {
        return route('tasks.edit', $id);
    }

    /** @test */
    public function authenticate_user_can_access_update_form()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $response = $this->get('/tasks/edit/'.$task->id);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.edit');
        $response->assertSee($task->name);
    }

    /** @test */
    public function unauthenticate_user_can_not_access_update_form()
    {
        $task = Task::factory()->create();
        $response = $this->get('/tasks/edit/'.$task->id);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('login'));
    }

    /** @test */
    public function authenticate_user_can_update_task_when_id_valid()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $updated = Task::factory()->make(['name' => 'updated'])->toArray();
        $response = $this->put('/tasks/update/'.$task->id,$updated);
        $response->assertStatus(Response::HTTP_FOUND);
        $response->assertRedirect(route('tasks.index'));
    }

    /** @test */
    public function unauthenticate_user_can_not_update_task_when_id_valid()
    {
        $task = Task::factory()->create();
        $updated = Task::factory()->make(['name' => 'updated'])->toArray();
        $response = $this->put('/tasks/update/'.$task->id,$updated);
        $response->assertRedirect(route('login'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_when_id_is_not_exist()
    {
        $this->actingAs(User::factory()->create());
        $task_id = -1;
        $updated = Task::factory()->make(['name' => 'updated'])->toArray();
        $response = $this->put('/tasks/update/'.$task_id,$updated);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test */
    public function authenticate_user_can_not_update_task_if_name_field_is_empty()
    {
        $this->actingAs(User::factory()->create());
        $task = Task::factory()->create();
        $updated =Task::factory()->make(['name' => null ])->toArray();
        $response = $this->from($this->updateTaskViewRoute($task->id))->put($this->updateTaskRoute($task->id));
        $response->assertRedirect($this->updateTaskViewRoute($task->id));
        $response->assertSessionHasErrors(['name']);
    }
}
